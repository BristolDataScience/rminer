1. Introduction
This software is an implementation of RMiner, an algorithm to mine patterns from multi-relational data, as described in:

Eirini Spyropoulou, Tijl De Bie and Mario Boley. Interesting Pattern Mining in Multi-Relational Data. Data Mining & Knowledge Discovery, 28(3): 808-849 (2014).

The selection of the interesting pattern set of k patterns is described in:
Tijl De Bie, Kleanthis-Nikolaos Kontonasios, Eirini Spyropoulou: A framework for mining interesting pattern sets. SIGKDD Explorations, 2010 12(2): 92-100.

2. Compilation
To compile run the compile.sh command file.

Note: If you are interested to see the maximum memory usage during runtime there is a small part of code that is specific to linux platforms (in RMiner.cpp and RMiner.h). This is by default commented out with a clear message. To turn memory reporting on, remove the comments from this part of the code and re-compile.

3. Data format
The data is formatted as follows. There is a one_to_one folder containing one file for every entity type. Every file lists all the entities of this type and associates the id of each entity with a string. These files can be either comma or tab separated. The first line of these files is of the form id,entity_type. There is also a binary folder containing all the relationships. Every file lists all relationship instances between the ids of the entities. These files are comma separated The first line of these files is of the form entity_type1,entity_type2.

4. Running
To run RMiner you need a configuration file. The configuration file has the following format (see config.txt for an example):

-i 0(to not compute interestingness)_or_1(to compute interestingness)
-r size_of_pattern_set_to_print
path_to_output_file
path_to_one_to_one_file1 constraint1
path_to_one_to_one_file2 constraint2
....
path_to_nary_folder

Note 1: Setting the parameters -i 1 and -r 0 will print all patterns ranked by interestingness. Setting -i 1 and -r k it will print an interesting set of k patterns. 
Note 2: The experimental evaluation of the efficiency(memory/speed) of RMiner presented in the related publications was done using -i 0 and -r 0.

5. Example
There is an example dataset in the folder and a configuration file related to it. To run RMiner on it: ./RMiner config.txt