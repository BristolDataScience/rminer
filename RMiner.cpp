/*=================================================================================================
 * RMiner version 1.2 - Software to mine interesting Maximal Complete Connected Subsets (MCCSs)
 * from multi-relational data.
 *
 * Copyright (C) 2011 Eirini Spyropoulou, Tijl De Bie
 * Department of Engineering Mathematics, University of Bristol, Bristol, UK
 * ------------------------------------------------------------------------------------------------
 *
 * RMiner-1.2 licensed under the Non-Profit Open Software License 3.0 (NPOSL-3.0)
 *================================================================================================*/

/** RMiner.cpp
 *
 *  Author: Eirini Spyropoulou
 *  email: eirini.spyropoulou@gmail.com
 **/

#include "RMiner.h"

using namespace std;

//constructor
RMiner::RMiner(vector<EntityList>* SData, vector<map<int, string> >* et, TypesAdjacency* at, vector<NullModel*> md, vector<vector<int> >* rt, double density, vector<int>* ct, vector<int>* od, bool comp_in){
        
    constraints=ct;
    order=od;
    
    G = SData;
    EntityNames = et;
    AdjTypes = at;
    RelsToTypes = rt;
    Models2 = md;
    compute_interestingness = comp_in;
    
    p = density;
    
    numOfClosedCCSs=0;
    numOfMCCSs=0;
    maxSpace=0;
    maxDepth=0;
    sizeOfMaxMCCS=0;
}

int RMiner::getNumOfMCCSs(){
    return numOfMCCSs;
}
int RMiner::getNumOfClosedCCss(){
    return numOfClosedCCSs;
}
int RMiner::getSizeOfMaxMCCS(){
    return sizeOfMaxMCCS;
}
int RMiner::getMaxDepth(){
    return maxDepth;
}
int RMiner::getMaxSpace(){
    return maxSpace;
}

//from the set of all NMCCSs it iteratively prints k, each time considering
//the self-information of the relationship instances not already presented to the user
//when k=0 it simply sorts and prints the whole list of patterns.
void RMiner::iteratively_print(int k, fstream& outfile){
	int i=0;
	
	vector<set<unsigned int> > entitiesAlreadyConveyed;
    set<unsigned int> set_int;
    
    if (k!=0){
        while (i<k && i<ML.size()){
            i++;
            isort sortingfunction;
            sort(MCCSsInfo.begin(), MCCSsInfo.end(), sortingfunction);
            int j = MCCSsInfo[0].patternIndex;
            outfile<<MCCSsInfo[0].interestingness<<" ";
            
            for (unsigned int m=0; m<ML[j].size(); m++){
                entitiesAlreadyConveyed.push_back(set_int);
                for (unsigned int sit=0; sit!=ML[j][m].size(); ++sit){
                    if (ML[j][m][sit]){
                        outfile<<(*EntityNames)[m][sit]<<" ";
                        entitiesAlreadyConveyed[m].insert(sit);
                    }
                }
            }
            
            outfile<<endl;
            MCCSsInfo.erase(MCCSsInfo.begin());
            for (unsigned int m=0; m<MCCSsInfo.size(); m++){
                int c = MCCSsInfo[m].patternIndex;
                MCCSsInfo[m].interestingness=self_info(ML[c],&entitiesAlreadyConveyed)/(MCCSsInfo[m].dlength);
            }
        }
    }
    else {
        if (compute_interestingness){
            isort sortingfunction;
            sort(MCCSsInfo.begin(), MCCSsInfo.end(), sortingfunction);
            for (unsigned int j=0; j<ML.size(); j++){
                int r=MCCSsInfo[j].patternIndex;
                outfile<<MCCSsInfo[j].interestingness<<" ";
                for (unsigned int m=0; m<ML[r].size(); m++){
                    for (unsigned int sit=0; sit!=ML[r][m].size(); ++sit){
                        if (ML[r][m][sit])
                            outfile<<(*EntityNames)[m][sit]<<" ";
                    }
                }
                outfile<<endl;
            }
        }
        else {
            for (unsigned int j=0; j<ML.size(); j++){
                for (unsigned int m=0; m<ML[j].size(); m++){
                    for (unsigned int sit=0; sit!=ML[j][m].size(); ++sit){
                        if (ML[j][m][sit])
                            outfile<<(*EntityNames)[m][sit]<<" ";
                    }
                }
                outfile<<endl;
            }
        }
    }
}

//checks whether the constraints are satisfied on the set Comp\(B U C) for every etnity type (constraint upper bound)
bool RMiner::eval_constraints(vector<set<unsigned int> >* Comp, vector<vector<bool> >* C, vector<vector<bool> >* B, vector<bool> reachable){
    
	set<unsigned int>::const_iterator cit;
	unsigned int card=0;
    
	for (unsigned int i=0; i<Comp->size(); i++){
		card=0;
		for (cit=(*Comp)[i].begin(); cit!=(*Comp)[i].end(); ++cit){
			if(!(*C)[i][*cit]){
				if (!(*B)[i][*cit])
					card++;
			}
			else {
				card++;
			}
			if (card >= (*constraints)[i])
				break;
		}
		if (card < (*constraints)[i] && reachable[i])
			return false;
	}
	return true;
}

//finds the additional self information of the relationship instances added due to entity j
//i is the type of the entity j, C is the CCS before adding j and Models2 contains the MaxEnt
//models for every relationship type
double RMiner::AddSelfInfo (int j, int i, vector<vector<bool> >* C, vector<NullModel*> Models2){
	double inf=0.0;
	int f;
	for (unsigned int t=0; t<(*AdjTypes)[i].size(); t++){
        if ((*AdjTypes)[i][t].size()>0){
            if (i==(*AdjTypes)[i][t][0])
                f=1;
            else
                f=0;
            for (unsigned int p=0; p<(*C)[(*AdjTypes)[i][t][f]].size(); p++){
                if ((*C)[(*AdjTypes)[i][t][f]][p]){
                    // Find the order of i and j;
                    if (f==1){
                        inf-=Models2[t]->logProbability(j,p);
                    }
                    else {
                        inf-=Models2[t]->logProbability(p,j);
                    }
                }
            }
        }
    }
	return inf;
}


//computes the self information of the MCCS C b, in the case the iterative output of patterns is used,
//which at every iteration takes into account only the self information of relationship instances not already conveyed to the user
double RMiner::self_info(vector<vector<bool> > C, vector<set<unsigned int> >* Conveyed){
	double si=0.0;
    
    for (unsigned int i=0; i<(*RelsToTypes).size(); i++){
        vector<int> entity_types = (*RelsToTypes)[i];
        for (unsigned int sit1=0; sit1!=(C)[entity_types[0]].size(); ++sit1){
            for (unsigned int sit2=0; sit2!=(C)[entity_types[1]].size(); ++sit2){
                if ((C)[entity_types[0]][sit1] && (C)[entity_types[1]][sit2]){
                    if (!((*Conveyed)[entity_types[0]].find(sit1)!=(*Conveyed)[entity_types[0]].end() && (*Conveyed)[entity_types[1]].find(sit2)!=(*Conveyed)[entity_types[1]].end()))
                        si-=Models2[i]->logProbability(sit1,sit2);
                }
            }
        }
    }
	return si;
}

//Finds the new set Comp. This is computed as an intersection between the previous Comp set and the Comp set of the newly added
//entity. In this function we make use of he fact that we know that for entity types not related to the entity types in the current CCS,
//the set Comp is going to contain all entities of this type in the dataset. Therefore we do not store explicitly entitties of this type.
//Rather the set Comp does not contain any entity of type not yet related to any types in the current CCS. We take this into account when
//computimg the intersection by keeping track of the entity types related to the current CCS (vector reachable) and the entity types related
//to the newly added entity. 
void RMiner::findNewComp(int type, int entity, vector<set<unsigned int> >* Comp, vector<set<unsigned int> >& newComp, vector<bool> reachable){
	bool isAdj;

    vector<set<unsigned int> >* rl = &(*G)[type][entity].relEntities;           //all entities related to the newly added entity

	set<unsigned int> temp;

	for (unsigned int s=0; s<(*Comp).size(); s++){                                 //for all entity types
		isAdj=false;
		for (unsigned int i=0; i<(*AdjTypes)[type].size() && !isAdj; i++){       //find if entity type is related to the entity type of the new entity
			for (unsigned int j=0; j<(*AdjTypes)[type][i].size() && !isAdj; j++){
				if ((*AdjTypes)[type][i][j]!=type){
					if (s==(*AdjTypes)[type][i][j]){
						isAdj = true;
						break;
					}
				}
			}
		}
		if (!isAdj) {                      //If the entity type is not related to the type of the newly added entity, the intersection of the Comp set of the
             if (!reachable[s])           //If it is not relted to any type of the current CCS
                 newComp.push_back(temp); //the Comp set for this type is empty
            else
                 newComp.push_back((*Comp)[s]); //current CCS and the Comp set of the entity, for this entity type, is equal to the Comp set of the current CCS.

		}
		else {
			newComp.push_back(temp);           //otherwise initialise with the empty set.
		}
	}
    
    set<unsigned int>::const_iterator cit;
    for (unsigned int k=0; k<(*rl).size(); k++){
        if (reachable[k]){                                  //if entity type already related to one of the types in the CCS
        
            insert_iterator<set<unsigned int> > ins_it(newComp[k], newComp[k].begin());
            set_intersection((*Comp)[k].begin(), (*Comp)[k].end(), (*rl)[k].begin(), (*rl)[k].end(), ins_it);
            
        }
        else  {                                          //if not related the intersection of all entities of this type and the related ones to
                                                        //the newly added entity is equal to the related entities to the newly added entity
            newComp[k]=(*rl)[k];
        }
    }
}

//returns bollean vector over entity types specifying whether an entity type should not be added yet due to
// the fact that types not satisfying the constraints have priority.
vector<bool> RMiner::findForbiddenTypes(vector<int>* constraints, vector<bool>* satPerType, vector<bool>* reachable){

	bool satisfied = true;
	vector<bool> res;
	vector<bool> temp;

	for (unsigned int i=0; i<constraints->size(); i++){
		if (!(*satPerType)[i] && (*reachable)[i])
			satisfied=false;

		res.push_back(false);                        //in case the constraints are satisfied all types are allowed
	}

	if (!satisfied){                                //in case they are not satisfied the types satisfying the constrainrs are not allowed
		for (unsigned int i=0; i<constraints->size(); i++){
			if ((*satPerType)[i]){
				res[i]=true;
			}

		}
	}
    
	return res;
}

//Check if element belongs to the closure by checking the equality of Comp sets before
//and after adding the elememt.
bool RMiner::checkIfInClosure(vector<set<unsigned int> >* Comp, vector<set<unsigned int> >* newComp){
    
    set<unsigned int>::const_iterator cit1;
    set<unsigned int>::const_iterator cit2;

	for (unsigned int t=0; t<G->size(); t++){

			if ((*newComp)[t].empty() && (*Comp)[t].empty()){

			}
			else {               
                if ((*newComp)[t].size() != (*Comp)[t].size())
                    return false;
                if (!equal((*newComp)[t].begin(),(*newComp)[t].end(), (*Comp)[t].begin()))
                    return false;
			}
	}

	return true;
}

//checks if C is maximal
bool RMiner::checkIfMaximal(vector<vector<bool> >* C, vector<set<unsigned int> >* Comp){
set<unsigned int>::const_iterator it;

	for (int i=0; i<Comp->size(); i++){
		for(it=(*Comp)[i].begin(); it!=(*Comp)[i].end(); ++it){
			if (!(*C)[i][(*it)])
				return false;
		}
	}
	return true;
}

//the actual algorithm C: a CCS, B:the set B as in the pseudocode, Comp:the set of compatible elements,
//reachable: boolean vector over all entity types containing 1 if there is at least one entity in C from an entity type related to it,
//ci: variable containing the self information and the description length of C,
//ct: vector of bollean values over all entity types specifying if the constraint for this entity type is satisfied,
//consat: boolean value specifying wheher the constraints have been met,
//depth: the depth of the search tree
void RMiner::run(vector<vector<bool> >* C, vector<vector<bool> >& B, vector<set<unsigned int> >* Comp, vector<bool> reachable, MCCSInfo ci, vector<bool> ct, bool consat, int depth){
    
	bool overlap;
	bool newconsSat = consat;
	vector<bool> consSatPerType=ct;
	vector<vector<bool> > newB = B;
    
    //*************************************************************
    /** lines to get the current usage of space (only for linux!!!)**/
	/*struct rusage usage;
	getrusage(RUSAGE_SELF, &usage);
	if (maxSpace<usage.ru_maxrss)
		maxSpace=usage.ru_maxrss;*/
    /****************************************************************/

    //check if there are entity types to be given priority for more efficient search
    //space pruning using the constraints
	vector<bool> ex = findForbiddenTypes(constraints, &consSatPerType, &reachable);
    
    int i;
    int j;
    
    set<unsigned int>::const_iterator mit;

	for (unsigned int a=0; a<order->size(); a++){                   //for all entity types in the Comp set (visited in order of increasing av. degree)
		i=(*order)[a];
		if ((reachable[i] || depth==0) && !ex[i]){                  //if in the set Aug and not excluded from the current recursion
		for (mit=(*Comp)[i].begin(); mit!=(*Comp)[i].end(); mit++){ //for all entities of this type in Comp
            
			if(!B[i][(*mit)]){                                      //if not in B then it is added to the current solution
                
                j=*mit;
                consSatPerType=ct;
                newconsSat = consat;
                vector<vector<bool> > newC = (*C);
                MCCSInfo newci;
                newci.info = ci.info;
                newci.entitiesIn = ci.entitiesIn;
                newci.entitiesOut = ci.entitiesOut;

                newC[i][j]=1;
                newB[i][j]=1;

                vector<set<unsigned int> > newComp;
                findNewComp(i, j, Comp, newComp, reachable);                            //computes the new set Comp

                vector<bool> r = reachable;
                for (unsigned int t=0; t<(*AdjTypes)[i].size(); t++){
                    for (unsigned int f=0; f<(*AdjTypes)[i][t].size(); f++){
                        if ((*AdjTypes)[i][t][f]!=i)
                            r[(*AdjTypes)[i][t][f]]=true;
                    }
                }
            
                if (compute_interestingness) {
                    newci.info+= AddSelfInfo(j, i, &newC, Models2);       //incrementally updates the self information
                }
                newci.entitiesIn=newci.entitiesIn+1;                     //the entities in the new C
                newci.entitiesOut=newci.entitiesOut-1;                   //and the entities in the new C
		            
                //initialisations before computing the closure
                overlap=false;
                double plusInfo=0;
                double pin=0;
                
                //check constraint upperbound
                bool upperbound;
                if (consat){
                    upperbound = true;
                }
                else {
                    upperbound = eval_constraints(&newComp, &newC, &newB, r);
                }

                if (upperbound){
                    vector<vector<bool> > closedNewC = newC;
                    vector<vector<bool> > closedNewB = newB;
                    if (depth!=0){
                        //compute the closure
                
                        set<unsigned int>::const_iterator it;
                        for (unsigned int n=0; n<newComp.size() && !overlap; n++){
                       
                            for (it=newComp[n].begin(); it!=newComp[n].end() && !overlap; ++it){ //for all entities in Aug
                                if(!closedNewC[n][*it]){
                                    vector<set<unsigned int> > Compnn;
                                    findNewComp(n, *it, &newComp, Compnn, r);

                                    bool c = checkIfInClosure(&newComp, &Compnn);
	
                                    if (c){
                                        closedNewC[n][*it]=1;
                                        closedNewB[n][*it]=1;
                                        
                                        if (compute_interestingness)
                                            plusInfo += AddSelfInfo(*it, n, &closedNewC, Models2);
                                        pin = pin+1;
                                        if (newB[n][*it]){   //closure overlapping with B
                                            overlap = true;
                                            break;
                                        }
                                    }
                                    Compnn.clear();
                                }
                            }
                        }
                    }

                    //check if the constraints are satisfied on the closed set
                    bool newconsat;
                    if (consat) {
                        newconsat=true;
                    }
                    else {
                        newconsat=true;
                        for (unsigned int d=0; d<closedNewC.size(); d++){
                            if (closedNewC[d].size()<(*constraints)[d])
                                newconsat=false;
                        }
                    }


                    if (!overlap){             //if there was no overlap with B
                        newci.info+=plusInfo;
                        numOfClosedCCSs++;
                        int new_depth=depth+1;

                        if (maxDepth<new_depth)
                			maxDepth=new_depth;

                        newci.entitiesIn= newci.entitiesIn+pin;
                        newci.entitiesOut=newci.entitiesOut-pin;
                        if (depth==0)
                            run(&closedNewC, closedNewB, &newComp, r, newci, consSatPerType, newconsSat, new_depth);
                        else{
                            if (checkIfMaximal(&closedNewC,&newComp)){  //check if MCCS
                                ML.push_back(closedNewC);
                                if (compute_interestingness) {
                                    newci.patternIndex = numOfMCCSs;
                                    newci.dlength = -newci.entitiesIn*log(p)-newci.entitiesOut*log(1-p);
                                    newci.interestingness=newci.info/newci.dlength;
                                    MCCSsInfo.push_back(newci);
                                }
                                numOfMCCSs++;
					
                                if (newci.entitiesIn>sizeOfMaxMCCS)
                                    sizeOfMaxMCCS=newci.entitiesIn;
                            }
                            else {
                                run(&closedNewC, closedNewB, &newComp, r, newci, consSatPerType, newconsSat, new_depth);
                            }
                        }
                    }
                    closedNewC.clear();
                    closedNewB.clear();
                }
                newComp.clear();
                newC.clear();
            }

        }
        }
	}
Comp->clear();
}

