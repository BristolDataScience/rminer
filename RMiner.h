/*=================================================================================================
 * RMiner version 1.2 - Software to mine interesting Maximal Complete Connected Subsets (MCCSs)
 * from multi-relational data.
 *
 * Copyright (C) 2011 Eirini Spyropoulou, Tijl De Bie
 * Department of Engineering Mathematics, University of Bristol, Bristol, UK
 * ------------------------------------------------------------------------------------------------
 *
 * RMiner-1.2 licensed under the Non-Profit Open Software License 3.0 (NPOSL-3.0)
 *=======/Users/eirini/Dropbox/RMiner_release/RMiner/RMiner.cpp=========================================================================================*/

/** RMiner.h
 *
 *  Author: Eirini Spyropoulou
 *  email: eirini.spyropoulou@gmail.com
 **/

#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <dirent.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <algorithm>
#include <iterator>
#include <ctime>
#include <sys/time.h>
//#include <sys/resource.h>
#include "DatabaseGenerator.h"
#include <math.h>


using namespace std;

///structure storing information
//about each entity.
typedef struct{
	vector<set<unsigned int> > relEntities; //related entities
	string field;
	string fieldid;
	int numOfrelEntities;
}Entity;

///structure holding information for the current solution.
//when an MMCCS is found the pointer points to that.
typedef struct{
	unsigned int patternIndex;
	double info;
	double entitiesIn;
	double entitiesOut;
	double dlength;
	double interestingness;
}MCCSInfo;

//sorting MCCSs based on interestingness
typedef struct isort {
    bool operator()(MCCSInfo i,MCCSInfo j) { return (i.interestingness>j.interestingness); }
}myisort;

//Vector of entities.
typedef vector<Entity> EntityList;
//Structure storing for every entity type and every relationship type it participates in all the entity types of this
//relationship type. If an entity type does not participate in a relationship type then this vector of entity types is
//empty.
typedef vector<vector<vector<int> > > TypesAdjacency;
//List of MCCSs. Every MCCS is a 2-dimentional vector containing all entities for every entity type.
typedef vector<vector<vector<bool> > > MCCSList;

class RMiner
{
    public:
    
    RMiner(vector<EntityList>* G, vector<map<int, string> >* et, TypesAdjacency* at, vector<NullModel*> md, vector<vector<int> >* rt, double density, vector<int>* ct, vector<int>* od, bool comp_in);
    
    void run(vector<vector<bool> >* C, vector<vector<bool> >& B, vector<set<unsigned int> >* Sc, vector<bool> reachable, MCCSInfo ci, vector<bool> ct, bool consSat, int depth);
    
    
	int getNumOfMCCSs();
    int getNumOfClosedCCss();
	int getSizeOfMaxMCCS();
	int getMaxDepth();
	int getMaxSpace();
    void iteratively_print(int k, fstream& outfile);
    
    private:
    
    int numOfClosedCCSs;
    int numOfMCCSs;
    long maxSpace;
    int maxDepth;
    int sizeOfMaxMCCS;
    
    MCCSList ML;
    vector<MCCSInfo> MCCSsInfo;
    vector<int>* constraints;
    vector<int>* order;
    
    vector<EntityList>* G;
    vector<map<int, string> >* EntityNames;
    TypesAdjacency* AdjTypes;
    vector<vector<int> >* RelsToTypes;
    vector<NullModel*> Models2;
    double p;
    bool compute_interestingness;
    
    void findNewComp(int type, int entity, vector<set<unsigned int> >* Comp, vector<set<unsigned int> >& newComp, vector<bool> reachable);
    vector<bool> findForbiddenTypes(vector<int>* constraints, vector<bool>* satPerType, vector<bool>* reachable);
    bool checkIfMaximal(vector<vector<bool> >* C, vector<set<unsigned int> >* Comp);
    double AddSelfInfo (int j, int i, vector<vector<bool> >* C, vector<NullModel*> Models2);
    bool eval_constraints(vector<set<unsigned int> >* Comp, vector<vector<bool> >* C, vector<vector<bool> >* B, vector<bool> reachable);
    bool checkIfInClosure(vector<set<unsigned int> >* Comp, vector<set<unsigned int> >* newComp);
    double self_info(vector<vector<bool> > C, vector<set<unsigned int> >* Conveyed);
};






