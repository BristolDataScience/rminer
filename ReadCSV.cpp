/*=================================================================================================
 * RMiner version 1.2 - Software to mine interesting Maximal Complete Connected Subsets (MCCSs)
 * from multi-relational data.
 *
 * Copyright (C) 2011 Eirini Spyropoulou, Tijl De Bie
 * Department of Engineering Mathematics, University of Bristol, Bristol, UK
 * ------------------------------------------------------------------------------------------------
 *
 * RMiner-1.2 licensed under the Non-Profit Open Software License 3.0 (NPOSL-3.0)
 *================================================================================================*/

/** ReadCSV.cpp
 *
 *  Author: Eirini Spyropoulou
 *  email: eirini.spyropoulou@gmail.com
 **/


#include <dirent.h>
#include <errno.h>
#include <time.h>
#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <vector>
#include <string>
#include <algorithm>
#include "RMiner.h"


using namespace std;

struct gsort {
  bool operator()(Entity i,Entity j) { return (i.numOfrelEntities<j.numOfrelEntities); }
}mygsort;

int main(int argc, char **argv) {

	vector<string> OnetoOne;
	vector<string> binary;
	vector<int> constraints;

	DIR *dp;
	struct dirent *dirp;

	string outfile;

/// Read Arguments
	bool compute_interestingness=true;
    int num_to_print=200;
    int argnumber = 0;
    
    FILE* fp;
    char file[100];
    char lineo[100];
    char cons[10];
	fp = fopen(argv[1], "r");
    char* p;
    char *l;
    
    if (fp==NULL){
        fprintf(stderr, "Can't read %s\n", argv[1]);
        exit(1);
    }
	while (fgets(lineo, 100, fp)!=NULL){ //read the config file
		argnumber++;
        bool isi=false;
        bool isr=false;
        
        l=strchr(lineo, '\r');
        if (l!=NULL)
            *l='\0';
        l=strchr(lineo, '\n');
        if (l!=NULL)
            *l='\0';
        
        if (argnumber==1 || argnumber==2){
            isi=false;
            isr=false;
            
            char* f=lineo;
            while (*f!=' '){
                if (*f=='-'){
                    
                }
                else if (*f=='i')
                    isi=true;
                else if (*f=='r')
                    isr=true;
                else{
                    cout<<"Please specify the -r and -i parameters in the config file."<<endl;
                    return errno;
                }
                f++;
            }
            int i=0;
            char n[20];
            while (*f!='\0'){
                if (isi){
                    if (*f=='1')
                        compute_interestingness=true;
                    else
                        compute_interestingness=false;
                }
                else if (isr){
                    n[i]=*f;
                }
                
                f++;
                i++;
            }
            if (isr){
                n[i]='\0';
                num_to_print=atoi(n);
                if (compute_interestingness==false)
                    num_to_print=0;
            }
        }
        else if (argnumber==3){ //output file
			outfile=lineo;
        }
		else {
            
            p=strchr(lineo, ' ');
            
            if (p!=NULL){ //one_to_one file with constraint
                
                int i=0;
                char* f=lineo;
                while (f!=p){
                    file[i] = *f;
                    i++;
                    f++;
                }
                file[i]='\0';
                OnetoOne.push_back(file);
                i=0;
                f++;
                while (*f!='\0'){
                    cons[i]=*f;
                    f++;
                    i++;
                }
                cons[i]='\0';
                constraints.push_back(atoi(cons));
            }
            else {
                
                if((dp  = opendir(lineo)) == NULL) {
                    cout << "Error(" << errno << ") opening directory: " <<lineo << endl;
                    return errno;
                }
                
                while ((dirp = readdir(dp)) != NULL) {
                    
                    if(string(dirp->d_name)==string(".") || string(dirp->d_name)==string("..") || string(dirp->d_name)==string(".svn"))
                        continue;
                    binary.push_back(string(lineo)+string(dirp->d_name));
                    
                }
                closedir(dp);
            }
        }
    }

/// Initialisations
	int entityTypeID=-1;
	int entityID=0;
	int RelType=-1;

	double NumOfEntities=0;
	double NumOfRelInsts=0;
	double NumOfPossibleRelInsts=0;
	double entityProb=0;

	TypesAdjacency TypesAdj;
	vector<NullModel*> Models2;
    vector<vector<int> > RelsToTypes;
	vector<EntityList> G;
	G.reserve(OnetoOne.size());

	map<string, int> EntityTypes;
	vector<map<string, int> >  EntityInitialIDs;
	EntityInitialIDs.reserve(OnetoOne.size());
	vector<map<string, int> >  EntityIDs;
	EntityIDs.reserve(OnetoOne.size());
	vector<map<int, string> >  EntityNames;
	EntityNames.reserve(OnetoOne.size());


	vector<double> RelInstsPerType;
	vector<double> EntitiesPerType;

	vector<string> fields;
	vector<string> types;

	char delim1='\n';
	char delim2=',';
	char delim3='\t';
	char delim;

	string line1, field;

	int f;

	time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    printf ( "Current local time and date: %s", asctime (timeinfo) );

//Initialise G
    map<string, int> ni;
    map<int, string> na;
    vector<Entity> nv;

	for (unsigned int a=0; a<OnetoOne.size(); a++){

        cout<<OnetoOne[a]<<endl;
		vector<vector<int> > tempv;
		vector<set<unsigned int> > temps;
		vector<int> tempi;

		fstream queryres1(OnetoOne[a].c_str(),ios::in);
		entityTypeID++;

		EntityInitialIDs.push_back(ni);
		G.push_back(nv);

		RelInstsPerType.push_back(0);

		for (unsigned int z=0; z<binary.size();z++){
			tempv.push_back(tempi);
		}
		tempi.clear();

		getline(queryres1,line1,delim1);
		stringstream instream(line1); //get first line

		while (getline(instream,field,delim2)){ //for all fields of first line
			if (field.find('\r')!=string::npos || field.find('\n')!=string::npos)
				field.erase(field.length()-1);
			types.push_back(field);
		}

		EntityTypes[types[1]]=entityTypeID;
		f=0;

		while (getline(queryres1,line1,delim1)){ //for every line
        
            TypesAdj.push_back(tempv);
			f=0;
			stringstream instream(line1);
			if (line1.find('\t')!=string::npos)
				delim=delim3;
			else
				delim=delim2;

			while (getline(instream, field,delim)){ //for all fields of every line
				if (field.find('\r')!=string::npos || field.find('\n')!=string::npos)
					field.erase(field.length()-1);
				fields.push_back(field);
			}
			if (fields.size()==2){
			Entity n;

			n.relEntities=temps;
			n.numOfrelEntities=0;
			n.field = types[1]+"."+fields[1];
			n.fieldid = fields[0];

			EntityInitialIDs[entityTypeID][fields[0]]=entityID;
			G[entityTypeID].push_back(n);
			entityID++;
			}

			fields.clear();
		}
		queryres1.close();
		types.clear();
        tempv.clear();
		EntitiesPerType.push_back(entityID);
		entityID=0;
	}

    //Count neighbours
	for (unsigned int c=0; c<binary.size(); c++){
        cout<<binary[c]<<endl;
		vector<int> entityTypeIDs;

		fstream queryres1(binary[c].c_str(),ios::in);

		getline(queryres1,line1,delim1);
		stringstream instream(line1); //get first line

		while (getline(instream,field,delim2)){ //for all fields of first line
			if (field.find('\r')!=string::npos || field.find('\n')!=string::npos)
				field.erase(field.length()-1);
			types.push_back(field); ///this should be types
		}

		for (unsigned int i=0; i<types.size(); i++){
			entityTypeIDs.push_back(EntityTypes[types[i]]);
		}

		RelsToTypes.push_back(entityTypeIDs);

		while (getline(queryres1,line1,delim1)){ //for every line

			stringstream instream(line1);
			if (line1.find('\t')!=string::npos)
				delim=delim3;
			else
				delim=delim2;

			while (getline(instream, field,delim)){ //for all fields of the line
				if (field.find('\r')!=string::npos || field.find('\n')!=string::npos)
					field.erase(field.length()-1);

				fields.push_back(field);
			}

			//for all types in the relationship, if all nodes exist, increase their neighbours
			bool allExist=true;
			for (unsigned int i=0; i<types.size(); i++){

				if (EntityInitialIDs[entityTypeIDs[i]].find(fields[i])== EntityInitialIDs[entityTypeIDs[i]].end()){
					allExist = false;
				}
			}

			if (allExist){
				for (unsigned int i=0; i<types.size(); i++){
						G[entityTypeIDs[i]].at(EntityInitialIDs[entityTypeIDs[i]][fields[i]]).numOfrelEntities++;
				}
			}

			fields.clear();
		}
		queryres1.close();
		types.clear();
	}

	//for every type and field store the index
	//run through the nary files to fill the neighbours.

	for (int i=0; i<entityTypeID+1; i++){
		vector<Entity> temp = G[i];

		vector<Entity> temp2;
		sort(temp.begin(), temp.end(), mygsort);

		int d=temp.size()-1;
		while (temp[d].numOfrelEntities > 0 && d>-1)
			d--;

		d++;

		temp2=vector<Entity>(temp.begin()+d, temp.end());
		G[i]=temp2;
	}

	set<unsigned int> temp;
	for (int i=0; i<entityTypeID+1; i++){//for all types
		EntityIDs.push_back(ni);
		EntityNames.push_back(na);
		for (unsigned int r=0; r<G[i].size(); r++){ // for all nodes

			for (int t=0; t<entityTypeID+1; t++){ //for all types of neighbours
				G[i][r].relEntities.push_back(temp);
			}
			EntityIDs[i][G[i][r].fieldid] = r;
			EntityNames[i][r]=G[i][r].field;

			NumOfEntities = NumOfEntities+1;
		}
	}
    
    NumOfRelInsts=0;
    NumOfPossibleRelInsts=0;

	for (unsigned int c=0; c<binary.size(); c++){
        RelType++;
		vector<int> sumsdim1;
        vector<int> sumsdim2;

		vector<int> entityTypeIDs;
		map<string, map<string, bool> > alreadyInNeighbourhood;
		fstream queryres1(binary[c].c_str(),ios::in);
		getline(queryres1,line1,delim1);
		stringstream instream(line1); //get first line

		while (getline(instream,field,delim2)){ //for all fields of first line
			if (field.find('\r')!=string::npos || field.find('\n')!=string::npos)
				field.erase(field.length()-1);
			types.push_back(field); ///this should be types
		}
        
        vector<int> adjv;
		for (unsigned int i=0; i<types.size(); i++){
			adjv.push_back(EntityTypes[types[i]]);
		}
        
        double num=1;
		for (unsigned int i=0; i<types.size(); i++){
			num = num * G[EntityTypes[types[i]]].size();
			entityTypeIDs.push_back(EntityTypes[types[i]]);
		}
        
        for (unsigned int j=0; j<G[EntityTypes[types[0]]].size(); j++){
            sumsdim1.push_back(0);
        }
        for (unsigned int j=0; j<G[EntityTypes[types[1]]].size(); j++){
            sumsdim2.push_back(0);
        }

		NumOfPossibleRelInsts+=num;

		while (getline(queryres1,line1,delim1)){ //for every line

			stringstream instream(line1);
			if (line1.find('\t')!=string::npos)
				delim=delim3;
			else
				delim=delim2;

			while (getline(instream, field,delim)){ //for all fields of the line
				if (field.find('\r')!=string::npos || field.find('\n')!=string::npos)
					field.erase(field.length()-1);
				fields.push_back(field);
			}

			//for all types in the relationship, if all nodes exist, push back their neighbours
			bool allExist=true;
			for (unsigned int i=0; i<types.size(); i++){
				if (EntityIDs[entityTypeIDs[i]].find(fields[i])== EntityIDs[entityTypeIDs[i]].end()){
					allExist = false;
				}
			}
			if (allExist){
                NumOfRelInsts++;
                sumsdim1[EntityIDs[entityTypeIDs[0]][fields[0]]]++;
                RelInstsPerType[entityTypeIDs[0]]++;
                G[entityTypeIDs[0]][EntityIDs[entityTypeIDs[0]][fields[0]]].relEntities[entityTypeIDs[1]].insert(EntityIDs[entityTypeIDs[1]][fields[1]]);
                TypesAdj[entityTypeIDs[1]][RelType]=adjv;
                
                sumsdim2[EntityIDs[entityTypeIDs[1]][fields[1]]]++;
                RelInstsPerType[entityTypeIDs[1]]++;
                G[entityTypeIDs[1]][EntityIDs[entityTypeIDs[1]][fields[1]]].relEntities[entityTypeIDs[0]].insert(EntityIDs[entityTypeIDs[0]][fields[0]]);
                TypesAdj[entityTypeIDs[0]][RelType]=adjv;
			}
			fields.clear();
		}

		NullModel* nm2;
        nm2 = new NullModel(&sumsdim1, &sumsdim2);
		Models2.push_back(nm2);
		
		queryres1.close();
        sumsdim1.clear();
        sumsdim2.clear();
		alreadyInNeighbourhood.clear();
		types.clear();
        adjv.clear();

	}

 entityProb = NumOfRelInsts/NumOfPossibleRelInsts;
cout<<"density:"<<(NumOfRelInsts/NumOfPossibleRelInsts)<<endl;

// Rank the types in order of the average number of related entities
// we use an order over the entity types based on average number
// of related entities and an order over the entities per type based on
// number of related entities as a proxy to a total order over the
//entites based on number of related entities.
		 bool swaped=true;
	 	 vector<int> order;
	 	 vector<double> MeanEdgesPerType;
	 	 for (int i=0; i<entityTypeID+1; i++){
	 		 order.push_back(i);
	 		 double d= RelInstsPerType[i]/EntitiesPerType[i];
	 		 MeanEdgesPerType.push_back(d);
	 	 }

	 	 while(swaped){
	 		 swaped=false;
	 		 for (int i=1; i<entityTypeID+1; i++){
	 			 if (MeanEdgesPerType[i]<MeanEdgesPerType[i-1]){
	 				 int a=order[i];
	 				 order[i]=order[i-1];
	 				 order[i-1]=a;
	 				 double b= MeanEdgesPerType[i];
	 				 MeanEdgesPerType[i]=MeanEdgesPerType[i-1];
	 				 MeanEdgesPerType[i-1]=b;
	 				 swaped=true;
	 			 }
	 		 }
	 	 }


	///Initialisations
    vector<vector<bool> > Cinit;
    vector<vector<bool> > B;
    vector<set<unsigned int> > Sc;
    vector<bool> r, ct;
    
    MCCSInfo ci;
    ci.info = 0;
	ci.entitiesIn = 0;
	ci.entitiesOut = NumOfEntities;
	ci.dlength = 0;
	ci.interestingness = 0;
    
    bool consSat = false;
    int d=0;
    
    vector<bool> tempv;
    set<unsigned int> temps;
    for (unsigned int i=0; i<G.size(); i++){
        Cinit.push_back(tempv);
        B.push_back(tempv);
        r.push_back(false);
        ct.push_back(false);
        Sc.push_back(temps);
        for (unsigned int j=0; j<G[i].size(); j++){            
            Sc[i].insert(j);
            B[i].push_back(false);
            Cinit[i].push_back(false);
        }
    }
    
    RMiner* m = new RMiner(&G, &EntityNames, &TypesAdj, Models2, &RelsToTypes, entityProb, &constraints, &order, compute_interestingness);
    
    clock_t begin = clock();
    m->run(&Cinit, B, &Sc, r, ci, ct, consSat, d);
    clock_t end = clock();

    double time_sec = (double(end - begin) / CLOCKS_PER_SEC);
    
    fstream outputfile(outfile.c_str(),ios::out);
    m->iteratively_print(num_to_print,outputfile);
	outputfile.close();
    
    /************** Print the statistics ********************/
	cout<<"time (sec): "<<time_sec<<endl;
	cout<<"MCCSs: "<<m->getNumOfMCCSs()<<endl;
    cout<<"closed CCSs: "<<m->getNumOfClosedCCss()<<endl;
	cout<<"Max MCCS: "<<m->getSizeOfMaxMCCS()<<endl;
	cout<<"Max Depth: "<<m->getMaxDepth()<<endl;
	cout<<"Max space: "<<m->getMaxSpace()<<endl;
	/*****************************************************/
}
