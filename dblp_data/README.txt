These data sets were created from the xml dump of dblp which can be downloaded from http://dblp.uni-trier.de/xml/.

The folder schema contains the data set used in the paper: 
Eirini Spyropoulou, Tijl De Bie and Mario Boley. Interesting Pattern Mining in Multi-Relational Data. Data Mining & Knowledge Discovery, 28(3): 808-849 (2014).
(Note that only papers from Data Mining and related venues were extracted to produce it).

The folder schema_nocitations contains an extracted dataset which does not contain any information about the paper citations. It also contains a slightly larger set of Data Mining and related venues.

The script used to extract schema_nocitations is extractRelations.pl and can be used as an example to extract other datasets from the xml dump.