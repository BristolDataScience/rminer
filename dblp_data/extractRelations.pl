#/usr/bin/perl
use strict;    # turn on compiler restrictions
use Switch;

my $file = "";
my %authorids;
my %paperids;
my %booksids;
my %yearids;
my $authorid = 0;
my $paperid = 0;
my $bookid = 0;
my $yearid = 0;
my $text = "";
my $typeid = 0;

my @authors = ();
my @citations = ();
my $authindex = 0;
my $citationindex = 0;
my $key = "";
my $title = "";
my $book = "";
my $year = "";
my $month = "";


open( INFILE, "dblp.xml" )
  or die("Can not open input file: $!");

open( AUTHPAPER1, ">schema_nocitations/many_to_many/authorPaper1.csv");
print AUTHPAPER1 "author,paper1\n";
#open( AUTHPAPER2, ">schema/many_to_many/authorPaper2.csv");
#print AUTHPAPER2 "author,paper2\n";
open( VENUEPAPER1, ">schema_nocitations/many_to_many/venuePaper1.csv");
print VENUEPAPER1 "venue,paper1\n";
#open( VENUEPAPER2, ">schema/many_to_many/venuePaper2.csv");
#print VENUEPAPER2 "venue,paper2\n";
open( YEARPAPER1, ">schema_nocitations/many_to_many/yearPaper1.csv");
print YEARPAPER1 "year,paper1\n";
#open( YEARPAPER2, ">schema/many_to_many/yearPaper2.csv");
#print YEARPAPER2 "year,paper2\n";
#open( PAPERPAPER, ">schema/many_to_many/Paper1Paper2.csv");
#print PAPERPAPER "paper1,paper2\n";
open( AUTHOR, ">schema_nocitations/one_to_one/author.csv");
print AUTHOR "id,author\n";
open( PAPER1, ">schema_nocitations/one_to_one/paper1.csv");
print PAPER1 "id,paper1\n";
#open( PAPER2, ">schema/one_to_one/paper2.csv");
#print PAPER2 "id,paper2\n";
open( YEAR, ">schema_nocitations/one_to_one/year.csv");
print YEAR "id,year\n";
open( VENUE, ">schema_nocitations/one_to_one/venue.csv");
print VENUE "id,venue\n";

#open (TOM, ">dblp_tom.txt");

while ( $file = <INFILE> ) {
	chomp($file);
	if ($file =~ "<article"){
		@authors = ();
		@citations = ();
		$key = "";
		$title = "";
		$book = "";
		$year = "";
		$month = "";
		$file =~ s/<article.*key="//;
		$file =~ s/">//;
		$key = $file;
		$authindex = 0;
		$citationindex = 0;
		$typeid = 1;
    	}
	elsif ($file =~ "<inproceedings"){
                $typeid = 2;
		@authors = ();
		@citations = ();
		$title = "";
		$book = "";
		$year = "";
		$file =~ s/<inproceedings.*key="//;
                $file =~ s/">//;
                $key = $file;
		$authindex = 0;
		$citationindex = 0;
        }
	elsif ($file =~ "</article>"){ #write all the info
		$typeid = 0;
		#if ($month ne ''){
		#	print TOM "$key,$title\t$month,$year\t";
		#	foreach my $i (@authors){
		#		print TOM "$i,";
		#	}
		#	print TOM "\n";
		#}
		 if ($book =~ "TKDD" || $book =~ m/Data Min. Knowl. Discov./ || $book =~ "m/SIGKDD Explorations/" || $book =~ m/IEEE Trans. Knowl. Data Eng./ || $book =~ m/Knowl. Inf. Syst./){
#		 if ( scalar(@citations)!=0){
                        print PAPER1 "$key,$title\n";
                        #print PAPER2 "$key,$title\n";
                        if (exists $yearids{$year}){
                                print YEARPAPER1 "$yearids{$year},$key\n";
                          #      print YEARPAPER2 "$yearids{$year},$key\n";
                        }
                        else {
                                $yearid = $yearid+1;
                                $yearids{$year}=$yearid;
                                print YEARPAPER1 "$yearids{$year},$key\n";
                            #    print YEARPAPER2 "$yearids{$year},$key\n";
                                print YEAR "$yearid,$year\n";
                        }
                        if (exists $booksids{$book}){
                                print VENUEPAPER1 "$booksids{$book},$key\n";
                              #  print VENUEPAPER2 "$booksids{$book},$key\n";
                        }
                        else {
                                $bookid = $bookid+1;
                                $booksids{$book}=$bookid;
                                print VENUEPAPER1 "$booksids{$book},$key\n";
                              #  print VENUEPAPER2 "$booksids{$book},$key\n";
                                print VENUE "$bookid,$book\n";
                        }

                        foreach my $i (@authors){
                                if (exists $authorids{$i}){
                                        print AUTHPAPER1 "$authorids{$i},$key\n";
                                     #   print AUTHPAPER2 "$authorids{$i},$key\n";
                                }
                                else {
                                        $authorid = $authorid+1;
                                        $authorids{$i}=$authorid;
                                        print AUTHPAPER1 "$authorid,$key\n";
                                      #  print AUTHPAPER2 "$authorid,$key\n";
                                        print AUTHOR "$authorid,$i\n";
                                }
                        }
                        foreach my $j (@citations){
                           #     print PAPERPAPER "$key,$j\n";
                        }

                }

        }        
	elsif ($file =~ "</inproceedings>") {
		$typeid = 0;
		if (index($book,'KDD')!=-1 || index($book,'PKDD')!=-1 || index($book,'ECML')!=-1 || index($book,'ICDM')!=-1 || index($book, 'PAKDD')!=-1 || index($book,'SDM')!=-1 || index($book,'CIKM')!=-1 || index($book, 'WSDM')!=-1 || index($book, 'Discovery Science')!=-1 || index($book, 'ICDE')!=-1){
		#if ($book eq "KDD" || $book eq "SDM" || $book eq "ICDM" || $book eq "PKDD" || $book eq "PAKDD" || $book =~ m/ECML\/PKDD/){
		#if (scalar(@citations)!=0 ){
			print PAPER1 "$key,$title\n";
		#	print PAPER2 "$key,$title\n";
			if (exists $yearids{$year}){
				print YEARPAPER1 "$yearids{$year},$key\n";
		#		print YEARPAPER2 "$yearids{$year},$key\n";
			}
			else {
				$yearid = $yearid+1;
				$yearids{$year}=$yearid;
				print YEARPAPER1 "$yearids{$year},$key\n";
                       #        print YEARPAPER2 "$yearids{$year},$key\n";
				print YEAR "$yearid,$year\n";
			}
			if (exists $booksids{$book}){
                               print VENUEPAPER1 "$booksids{$book},$key\n";
                        #       print VENUEPAPER2 "$booksids{$book},$key\n";
                       }
                       else {
                               $bookid = $bookid+1;
                               $booksids{$book}=$bookid;
                               print VENUEPAPER1 "$booksids{$book},$key\n";
                         #      print VENUEPAPER2 "$booksids{$book},$key\n";
                               print VENUE "$bookid,$book\n";
                       }

			foreach my $i (@authors){
				if (exists $authorids{$i}){
					print AUTHPAPER1 "$authorids{$i},$key\n";
				#	print AUTHPAPER2 "$authorids{$i},$key\n";
				} 
				else {
					$authorid = $authorid+1;
					$authorids{$i}=$authorid;
					print AUTHPAPER1 "$authorid,$key\n";
                               #        print AUTHPAPER2 "$authorid,$key\n";
					print AUTHOR "$authorid,$i\n";
				}
			}
			foreach my $j (@citations){
			#	print PAPERPAPER "$key,$j\n";
			}
					
        	}
	}
	elsif ($file =~ "<author>"){
		my $author = "";
		if ($typeid==1 || $typeid==2){	
			$file =~ s/<author>//;
			$file =~ s/<\/author>//;
			$author=$file;
			$authors[$authindex]=$author;
			$authindex=$authindex+1;
		}
	}
	elsif ($file =~ "<cite>"){
		my $citation="";
                if ($typeid==1 || $typeid==2){  
                        $file =~ s/<cite>//;
                        $file =~ s/<\/cite>//;
                        $citation=$file;
			if ($citation !~ m/\.\.\./){
                        	$citations[$citationindex]=$citation;
                        	$citationindex=$citationindex+1;
			}
                }
        }
	elsif ($file =~ "<title>"){
		if ($typeid==1 || $typeid==2){
			$file =~ s/<title>//;
                	$file =~ s/<\/title>//;
                	$title=$file;
		}

	}
	elsif ($file =~ "<year>"){
                if ($typeid==1 || $typeid==2){
                        $file =~ s/<year>//;
                        $file =~ s/<\/year>//;
                        $year=$file;
                }

        }
	elsif ($file =~ "<month>"){
                if ($typeid==1 || $typeid==2){
                        $file =~ s/<month>//;
                        $file =~ s/<\/month>//;
                        $month=$file;
                }

        }
	elsif ($file =~ "<booktitle>"){
		if ($typeid == 2){
			$file =~ s/<booktitle>//;
                        $file =~ s/<\/booktitle>//;
                        $book=$file;
		}
	}
	elsif ($file =~ "<journal>"){
                if ($typeid == 1){
                        $file =~ s/<journal>//;
                        $file =~ s/<\/journal>//;
                        $book=$file;
                }
        }

}


close(AUTHPAPER1);
close(AUTHPAPER2);
close(VENUEPAPER1);
close(VENUEPAPER2);
close(YEARPAPER1);
close(YEARPAPER2);
close(PAPERPAPER);
close(AUTHOR);
close(YEAR);
close(VENUE);
close(PAPER);
